![Postman logo](postman.svg)

# postman.desktop

A desktop file for the postman application as needed for GNOME/KDE integration

The Linux install of the Postman app does not include a .desktop file nor a logo. This is a version which works.

## Installation

In the interests of keeping unofficial resources separate from anything installed from a .deb, .rpm or similar file everything should be in the /usr/local space. Yes, I know you could cd to that directory but I've just added the full paths to make it easy.

### Postman app
Download the official Postman app (try [https://www.postman.com/downloads/](https://www.postman.com/downloads/) ) and move it into place:

`sudo mv /path/to/downloaded/Postman /usr/local/bin/Postman.app`

Make a symbolic link to the runtime so that it isn't too nested:

`sudo ln -s /usr/local/bin/Postman.app/app/Postman /usr/local/bin/postmane`

### Postman logo

It's best to use a scaleable SVG file for the postman logo so download this:

`sudo mkdir -pv /usr/local/share/icons/hicolor/scalable/apps
sudo curl https://gitlab.com/rolandw/postman.desktop/-/raw/master/postman.svg?inline=false > /usr/local/share/icons/hicolor/scalable/apps/postman.svg`

### Postman desktop file

Although there are tools to install .desktop files in some distributions, they're not available in all so just download the desktop file

`sudo curl https://gitlab.com/rolandw/postman.desktop/-/raw/master/postman.desktop?inline=false > /usr/local/share/applications/postman.desktop`

### Finally

If Postman doesn't appear as an application for you, log out and log back in again...

